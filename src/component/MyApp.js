import React from "react";
import { Link } from "react-router-dom";

export const SecondComponent = () => {
  const links=[
    // {
    //   name:"Filter",
    //   link:'/filter'
    // },
    // {
    //   name:"Dashboard",
    //   link:'/dashboard'
    // },
    {
      name:"Dashboard",
      link:'/dashboard/'
    },
    {
      name:"user Dashboard",
      link:'/dashboard/user'
    }
  ]
 
  return (
    <div>
     {
      links.map((item)=>(
        <div key={item.link}>
          <Link to ={`${item.link}`}>
          {item.name}
          </Link>
        </div>

      ))
     }
    </div>
  );
};
