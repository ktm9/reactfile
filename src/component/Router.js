import { lazy } from "react";
import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import { SecondComponent } from "./MyApp.js";
import { FilterComponent } from "./array/FilterComponent";
import DashBoardLayout from "./layout/DashBoardLayout.js";
// import UserDashBoard from "./UserDashBoard.js";
// import Dashboard from "./Dashboard";
const Dashboard=lazy(()=> import('./Dashboard.js'))
// const FilterComponent =lazy(()=> import('./array/FilterComponent'))
const User=lazy(()=> import('./Dashboard.js'))
const UserDashBoard=lazy(()=> import('./UserDashBoard.js'))


// Configure nested routes with JSX
export const Routers=createBrowserRouter(
    createRoutesFromElements(
      

      <Route >
        <Route path="/" element={<SecondComponent/>}/>

         <Route path="/filter" element={<FilterComponent/>}/>
         <Route  path="/dashboard" element={<DashBoardLayout/>}>
          <Route index element={<Dashboard/>}/>
          <Route path="user" element={<UserDashBoard/>}/>

         </Route>
         
        
        
        
      </Route>
    )
  );