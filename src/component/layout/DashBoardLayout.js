import React from 'react'
import { Outlet } from 'react-router-dom'

const DashBoardLayout = () => {
  return (
    <div>DashBoard Layout
      <div>
        <Outlet/>
      </div>
    </div>
  )
}

export default DashBoardLayout