import React from "react";

export const LearnArray = () => {
    const country = [
        {
            name: "United States",
            city: "california",
            id: 1,
        },
        {
            name: "India",
            city: "delhi",
            id: 2,
        },
        {
            name: "China",
            city: "beijing",
            id: 3,
        },
        {
            name: "Japan",
            city: "tokyo",
            id: 4,
        },
    ];

    return (
        <div>
            {country.map((item) => {
                if(item.id === 1 ){
                    return(<div key={item.id}>
                        <div>Name:{item.name}</div>
                        <div>City:{item.city}</div>
                        <div>Zipcode:{item.id}</div>
                    </div>)
                }
                
            })}
        </div>
    );
};
